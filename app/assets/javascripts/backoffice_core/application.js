// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// the compiled file.
//
// WARNING: THE FIRST BLANK LINE MARKS THE END OF WHAT'S TO BE PROCESSED, ANY BLANK LINE SHOULD
// GO AFTER THE REQUIRES BELOW.
//
//= require jquery
//= require jquery_ujs
//= require jquery-ui
//= require_tree .
$(document).ready(function() {
  // rozbalovani leveho menu
  $(".menu_side > li ul").hide();
  $(".menu_side > li").click(function() {
    $("ul", this).toggle(100);        
  });  
  
  // naseptavac mesta
  $("#user_city, #company_city").keyup(function() {
    var url = "/backoffice_core/select_cities/" + $(this).val() + ".json";
    $.getJSON(url, function(cities) {
      $( ".city_completion" ).autocomplete({
        source: cities
      });    
    });
  });
});

function deleteFormErrors(tagId) {
  $(tagId + ' .error').remove();
};

function pleaseWait(tagId) {
  $(tagId).html('Sending the form...');
}


