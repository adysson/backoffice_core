// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.
function add_company_detail(i) {
  var nextI = i + 1;
  
  $(".acd").before("<input type=\"text\" name=\"new_company_details[" + i + "][title]\" value=\"\" placeholder=\"typ údaje\" class=\"input_company_detail_type\" >");
  $(".acd").before("&nbsp;<input type=\"text\" name=\"new_company_details[" + i + "][detail_value]\" value=\"\" placeholder=\"údaj\" class=\"input_company_detail_value\"><br>");
  $(".acd").before("&nbsp;<input id=\"new_company_details_" + i + "_recursive\" type=\"checkbox\" name=\"new_company_details_recursive[" + i + "]\">");
  $(".acd").before("&nbsp;<label for=\"new_company_details_" + i + "_recursive\">propsat do všech</label><br>");
  $(".acd").attr("href", "javascript:add_company_detail(" + nextI + ");");
}

function add_employee_post(i) {
  var nextI = i + 1;
  
  $(".employee_posts_list .add_employee_post").before("<label for=\"employee_posts_" + i + "_title\">" + i + ". - </label>");
  $(".employee_posts_list .add_employee_post").before("<input type=\"text\" id=\"employee_posts_" + i + "_title\" name=\"employee_posts[" + i + "][title]\" /><br><br>")
  $(".add_employee_post").attr("href", "javascript:add_employee_post(" + nextI + ");");
}

function remove_user_field(i) {
  $("#user_post_" + i).remove();
}
 
function add_company_user(employee_post_id, employee_post_title, license_options) {
  var userCount = $("#users_count").attr("value");
  var i = parseInt(userCount);
  
  $("#users_count").attr("value", i + 1);
  // adds neccessary HTML code
  $(".aep_" + employee_post_id).before("<div id=\"user_post_" + i + "\"></div>");
  $("#user_post_" + i).append("<br><label for=\"users_" + i + "_name\">" + employee_post_title + ":</label>");
  $("#user_post_" + i).append("&nbsp;<input class=\"w100\" id=\"users_" + i + "_name\" name=\"users[" + i + "][name]\" placeholder=\"Jméno\" type=\"text\" />");
  $("#user_post_" + i).append("&nbsp;<input class=\"w100\" id=\"users_" + i + "_surname\" name=\"users[" + i + "][surname]\" placeholder=\"Příjmení\" type=\"text\" />");
  $("#user_post_" + i).append("&nbsp;<input class=\"w150\" id=\"users_" + i + "_email\" name=\"users[" + i + "][email]\" placeholder=\"E-mail\" type=\"text\" />");
  $("#user_post_" + i).append("<input id=\"users_" + i + "_employee_post_id\" name=\"users[" + i + "][employee_post_id]\" type=\"hidden\" value=\"" + employee_post_id + "\" />");
  $("#user_post_" + i).append("<br><label class=\"ml50\" for=\"license\">Licence:</label>");
  $("#user_post_" + i).append("&nbsp;<select class=\"w100\" id=\"users_" + i + "_license_id\" name=\"users[" + i + "][license_id]\">" + license_options + "</select>");
  $("#user_post_" + i).append("<a href=\"javascript:remove_user_field(" + i + ");\">Odebrat</a>");
}

function forwardTitleToTreeNode(nodeId) {
  var nodeSelector = "#node" + nodeId;
  var inputSelector = "#companies_" + nodeId + "_title";
  
  $(nodeSelector + " > div > h3").text( $(inputSelector).val() );
}

