// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.
$(document).ready(function() {
  $("#companies").change(function() {
    var url = "/backoffice_core/return_company_users/" + $(this).val();
    
    $.getJSON(url, function(users) {
      $("#users_for_membership").empty();

      for(var i = 0; i < users.length; i++) {
        $("#users_for_membership").append("<option value=\"" + users[i].id + "\">" + users[i].name + "</option>");
      }
    });
  })

  $("#add_to_group").click(function() {
    $("#users_for_membership option:selected").each(function() {
      $("#members, #users_for_admin").append("<option value=\"" + $(this).val() + "\">" + $(this).text() + "</option>");
    });
  });
  
  $("#remove_member").click(function() {
    $("#members option:selected").each(function() {
      $(this).remove();
      $("#users_for_admin option[value=" + $(this).val() + "]").remove();
      $("#admins option[value=" + $(this).val() + "]").remove();
    });
  });
  
  $("#add_admin").click(function() {
    $("#users_for_admin option:selected").each(function() {
      $("#admins").append("<option value=\"" + $(this).val() + "\">" + $(this).text() + "</option>");
      $("#users_for_admin option[value=\"" + $(this).val() + "\"]").remove();
    });
    
  })
  
  $("#remove_admin").click(function() {
    $("#admins option:selected").each(function() {
      $("#users_for_admin").append("<option value=\"" + $(this).val() + "\">" + $(this).text() + "</option>")
      $(this).remove();
    });
    
  });
  
  // aby se ulozili vsichni vybrani a umisteni v selectboxech, je treba je pred odeslanim vybrat
  $("#new_group, .edit_group").submit(function() {
    $("#members option, #admins option").each(function() {
      $(this).attr("selected", "selected");
    });
  });
});

