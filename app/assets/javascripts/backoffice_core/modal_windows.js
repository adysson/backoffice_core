var modalWindow = {  
    parent:"body",  
    windowId:null,  
    content:null,  
    width:null,  
    height:null,  
    close:function()  
    {  
        $(this.windowId).css("display", "none");
        $("#modal_overlay").css("display", "none");
    },  
    open:function()  
    {  
		$(this.windowId).css("display", "block");
		$("#modal_overlay").css("display", "block");

        $(".modal_window").append("<a class=\"close_window\"></a>");  
        $(".close_window").click(function(){modalWindow.close();});  
        $("#modal_overlay").click(function(){modalWindow.close();});  
    }  
}; 

var openMyModal = function(source)  
{
	modalWindow.windowId = source;
    modalWindow.open();  
};
