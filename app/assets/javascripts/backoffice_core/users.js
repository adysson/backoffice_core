// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.
$(document).ready(function() {
  $(".datepicker").datepicker({ changeYear: true,
                                  dateFormat: "dd. mm. yy",
                                  dayNamesMin: [ "Ne", "Po", "Út", "St", "Čt", "Pá", "So" ],
                                  firstDay: 1,
                                  monthNames: [ "Leden", "Únor", "Březen", "Duben", "Květen", "Červen", "Červenec", "Srpen", "Září", "Říjen", "Listopad", "Prosinec"],
                                  yearRange: "c-100:c" });
});


