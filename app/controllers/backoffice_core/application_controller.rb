#!/bin/env ruby
# encoding: utf-8

module BackofficeCore
  class ApplicationController < ActionController::Base
    before_filter :current_user, :authorize, :set_mailer_host

    rescue_from CanCan::AccessDenied do |exception|
      flash[:error] = "Nemáte oprávnění."
      redirect_to :back
    end
    
    protected
  
    def authorize
      unless User.find_by_id(session[:user_id])
        redirect_to sign_in_url, alert: "Prosím přihlašte se. Pokud nemáte uživatelský účet, zaregistrujte se."
      end
    end
    
    # workaround for use CanCan in engine
    def current_ability
      BackofficeCore::Ability.new(current_user)
    end
    
    def current_user
      if session[:user_id]
        @current_user = User.find(session[:user_id])
      else
        @current_user = User.new # have to create new empty object
      end
    end
    
    def set_mailer_host
      ActionMailer::Base.default_url_options = { host: request.host_with_port }
    end
  end
end
