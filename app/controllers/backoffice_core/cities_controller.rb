#!/bin/env ruby
# encoding: utf-8

require_dependency "backoffice_core/application_controller"

module BackofficeCore
  class CitiesController < ApplicationController
    def select_cities
      if params[:city_title].length > 0
        @cities = City.where("title LIKE '" + params[:city_title] + "%'").group("title").map { |c| c.title }
        
        respond_to do |format|
          format.json { render json: @cities.to_json.force_encoding("utf-8") }
        end
      end
    end
  end
end
