#!/bin/env ruby
# encoding: utf-8

require_dependency "backoffice_core/application_controller"

module BackofficeCore
  class CompaniesController < ApplicationController
    respond_to :js # prepares for respond_with
    rescue_from CanCan::AccessDenied do |exception|
      redirect_to settings_url, notice: 'Nemáte oprávnění!'
    end
    
    def show
      @company = @current_user.company
      @company_list = @company.tree.one_level_array
      @company_hash = @company.children_companies_hash(@company)      
      @c_child = Company.find(20)
      @c_parent = Company.find(10)
      @resultat = @c_child.is_child_of? @c_parent
      
      @i = 0
    end
    
    def create
      if Company.find(params[:parent_company_id])
        @new_company = Company.new(parent_company_id: params[:parent_company_id])

        flash[:notice] = "Pobočka byla vytvořena." if @new_company.save
        respond_with(@new_company, layout: !request.xhr?)      
      end
    end
    
    def destroy
      c = Company.find(params[:id])
      @deleted_company_id = c.id
      @parent_company_id = c.parent_company_id || nil
      
      flash[:notice] = "Pobočka byla smazána." if c.destroy
      respond_with(@deleted_company_id, layout: !request.xhr?)
    end
    
    # NON RESTFULL setting up new company process:
    # 1. company_structure -> company_structure_update
    # 2. company_details -> company_details_update
    # 3. company_employee_posts -> company_employee_post_update
    # 4. company_users -> company_users_update
    # 5. company_finish
  
    def company_structure
      @current_user.create_default_company if @current_user.company.blank?
      @paid_licenses = License.all
      @company = @current_user.company
      @company_types = CompanyType.all.map { |ct| [ct.title, ct.id] }
      @error_fields = []

      authorize! :manage, @company
    end
    
    def company_structure_update
      redirect_to company_structure_url if @current_user.company.blank?
      @company = @current_user.company
      @company_types = CompanyType.all.map { |ct| [ct.title, ct.id] }
      @error_fields = [] # push ids of untitled companies here

      if params[:companies].any?  
        params[:companies].values.each do |company|
          c = Company.find(company[:id])
          if company[:title].present?
            c.update_attribute('title', company[:title])
            c.manage_automatic_group(@current_user) # creates or renames company automatic group
          else
            @error_fields << company[:id]
          end
        end
        
        if @error_fields.empty?
          redirect_to company_details_url
        else        
          @paid_licenses = License.all
          @company = @current_user.company
          @company_types = CompanyType.all.map { |ct| [ct.title, ct.id] }
          @company.errors.add(:title, "nesmí být prázdný")
          
          render template: '/backoffice_core/companies/company_structure'
        end
      end    
      authorize! :manage, @company
    end
    
    def company_details
      redirect_to company_structure_url if @current_user.company.blank?
      @paid_licenses = License.all
      
      if params[:select_company_id]
        @company = Company.find(params[:select_company_id])
      else
        @company = @current_user.company
      end
      @companies_for_select = @current_user.company.tree.one_level_array.map { |c| [c.title, c.id] }
      @company_editation = true # starts city field completion
      @cities = City.all.map { |c| c.title }
      @countries = Country.all.map { |c| [c.title, c.id] }
      
      authorize! :manage, @company
    end
    
    def company_details_update
      redirect_to company_structure_url if @current_user.company.blank?
      @company = Company.find(params[:selected_company_id])
      @a = params[:company_details]
      @b = params[:recursive_details]
      @c = params[:new_company_details]
      @d = params[:new_company_details_recursive]
    
      if @company.update_attributes(params[:company])
        @current_user.logs.create(title: 'Uživatel uložil popis společnosti (2. krok nastavení).')
        @company.save_params_to_all_subcompanies(params[:recursive], params[:company])

        if params[:new_company_details].present?
          @company.save_new_company_details(params[:new_company_details],
                                            params[:new_company_details_recursive])
        end
        @company.save_existing_company_details(params[:company_details], params[:recursive_details]) if params[:company_details].present?

        redirect_to company_employee_posts_url
      else
        @paid_licenses = License.all
        @companies_for_select = @current_user.company.tree.one_level_array.map { |c| [c.title, c.id] }
        @company_editation = true # starts city field completion
        @cities = City.all.map { |c| c.title }
        @countries = Country.all.map { |c| [c.title, c.id] }

        render template: '/backoffice_core/companies/company_details'
      end
      authorize! :manage, @company
    end
    
    def company_employee_posts
      redirect_to company_structure_url if @current_user.company.blank?
      @paid_licenses = License.all
      @company = @current_user.company
      @employee_posts = @company.top_parent_company.employee_posts.all.map { |ep| [ep.title, ep.id] }
      authorize! :manage, @company
    end
    
    def company_employee_posts_update
      redirect_to company_structure_url if @current_user.company.blank?
      @company = @current_user.company
      @output = []

      params[:employee_posts].each do |ep|
        defined_employee_post = EmployeePost.where("company_id = #{@company.top_parent_company.id} AND id = #{ep[0]}")
        if defined_employee_post.blank? and ep[1][:title].present?
          @company.top_parent_company.employee_posts.create(ep[1])
        elsif ep[1][:title].present?
          @company.top_parent_company.employee_posts.where("company_id = #{@company.top_parent_company.id} AND id = #{ep[0]}").first.update_attribute(:title, ep[1][:title])
        end
      end
      @current_user.logs.create(title: 'Uživatel uložil funkce ve společnosti (3. krok nastavení)')
      redirect_to company_users_url
      authorize! :manage, @company
    end
    
    def company_users
      if params[:select_company_id]
        @company = Company.find(params[:select_company_id])
      else
        @company = @current_user.company
      end
      @paid_licenses = License.all
      @top_company = @current_user.company
      @companies_for_select = @current_user.company.tree.one_level_array.map { |c| [c.title, c.id] }
      @licenses = License.all.map { |l| [l.title, l.id] }
      @employee_posts = @current_user.company.top_parent_company.employee_posts.all
      @users_count = @company.top_parent_company.count_whole_company_users
      @i = 0 # universal iterator for new & existing user's employee posts 

      authorize! :manage, @company
    end
    
    def company_users_update
      if params[:selected_company_id]
        @company = Company.find(params[:selected_company_id])
      else
        @company = Company.find(params[:company_id])
      end
      @companies_for_select = @current_user.company.tree.one_level_array.map { |c| [c.title, c.id] }
      @licenses = License.all.map { |l| [l.title, l.id] }
      @employee_posts = @current_user.company.employee_posts.all
      
      params[:users].values.each_with_index do |u, i|
        if u[:id].present?
          user = User.find(u[:id])
          user.update_attributes(u)
        elsif u[:email].present?
          u[:company_id] = @company.id
          begin
            user = User.new(u)
            user.register_by_chief # creates new user and starts necessary actions
          rescue ActiveRecord::RecordInvalid => e
            u[:errors] = e
          end
        end
        
        # adds user to the company automatic group
        if user.present?
          @company.group.add_member(user)
        end
      end
      @current_user.logs.create(title: 'Uživatel nastavil uživatele ve společnosti (4. krok nastavení)')
      
      if params[:next]
        redirect_to company_finish_url
      else
        redirect_to :back
      end
      authorize! :manage, @company
    end
    
    def company_finish
      @company = @current_user.company
      @company_finish = true # inserts calculations modal window
      @users_posts = @company.whole_company_users_count_by_posts
      @paid_licenses = License.all
      @ep_options = @company.employee_posts.map { |ep| [ep.title, ep.id] }
      @company_options = @company.tree.one_level_array.map { |c| [c.title, c.id] }
      @license_options = License.all.map { |l| [l.title, l.id] }
    end
    
    def company_finish_update
      @company = @current_user.company

      if params[:users].any?
        params[:users].each do |id, param|
          u = User.find(id)
          u.company.group.remove_member(u)
          u.update_attributes(param)
          u.company.group.add_member(u)
        end
        redirect_to company_finish_url
      else
        @company_finish = true # inserts calculations modal window
        @users_posts = @company.whole_company_users_count_by_posts
        @paid_licenses = License.all
        @ep_options = @company.employee_posts.map { |ep| [ep.title, ep.id] }
        @company_options = @company.tree.one_level_array.map { |c| [c.title, c.id] }
        @license_options = License.all.map { |l| [l.title, l.id] }

        render template: '/backoffice_core/companies/company_finish'
      end
    end
    
    def return_company_users
      if params[:company_id]
        company = Company.find(params[:company_id])
        @users = company.users.map { |u| { name: u.whole_name, id: u.id } }
        
        respond_to do |format|
          format.json { render json: @users.to_json }
        end
      end
    end
  end
end
