#!/bin/env ruby
# encoding: utf-8

require_dependency "backoffice_core/application_controller"

module BackofficeCore
  class ContactEmailsController < ApplicationController
    def new
      @contact_email = ContactEmail.new
    end
    
    def create
      @contact_email = ContactEmail.new(params[:contact_email])
      
      respond_to do |format|
        if @contact_email.save
          @current_user.logs.create(title: 'Uživatel přidal nový email.')
          format.html { redirect_to edit_user_url @current_user, notice: 'Nový email uložen.' }
        else
          format.html { redirect_to edit_user_url @current_user, error: 'Nepodařilo se uložit email.' }
        end
        format.js
      end        
    end
  end
end
