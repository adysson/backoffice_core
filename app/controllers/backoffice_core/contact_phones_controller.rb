#!/bin/env ruby
# encoding: utf-8

require_dependency "backoffice_core/application_controller"

module BackofficeCore
  class ContactPhonesController < ApplicationController
    def new
      @contact_phone = ContactPhone.new
    end
    
    def create
      @contact_phone = ContactPhone.new(params[:contact_phone])
      
      respond_to do |format|
        if @contact_phone.save
          @current_user.logs.create(title: 'Uživatel přidal nové telefonní číslo.')
          format.html { redirect_to edit_user_url @current_user, notice: 'Nový telefon uložen.' }
        else
          format.html { redirect_to edit_user_url @current_user, error: 'Nepodařilo se uložit telefon.' }
        end
        format.js
      end        
    end
  end
end
