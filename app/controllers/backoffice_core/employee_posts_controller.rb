#!/bin/env ruby
# encoding: utf-8

require_dependency "backoffice_core/application_controller"

module BackofficeCore
  class EmployeePostsController < ApplicationController
    def destroy
      employee_post = EmployeePost.find_by_id(params[:id])
      
      if employee_post.destroy
        redirect_to :back
      else
        redirect_to :back, alert: 'Nepodařilo se smazat vybranou funkci.'
      end
    end
  end
end
