#!/bin/env ruby
# encoding: utf-8

require_dependency "backoffice_core/application_controller"

module BackofficeCore
  class GroupsController < ApplicationController
    def index
      @groups = Group.all
    end
  
    def new
      @group = Group.new
      @companies_for_select = @current_user.company.tree.one_level_array.map { |c| [c.title, c.id] }
      @users_for_membership = @current_user.company.whole_company_users.map { |u| [u.whole_name, u.id] }
      @users_for_admin = []
      @members = []
      @admins = []
    end
    
    def create
      if params[:group]# and params[:group][:title].length > 0
        @group = Group.new(params[:group])
        
        if @group.save
          params[:members].each { |m| @group.add_member(User.find(m)) } if params[:members].present?
          params[:admins].each { |a| @group.add_admin(User.find(a)) } if params[:admins].present?
          
          Log.create(title: 'Uživatel vytvořil skupinu ' + @group.title)
          redirect_to groups_url, notice: 'Skupina je uložena.'
        else
          @companies_for_select = @current_user.company.tree.one_level_array.map { |c| [c.title, c.id] }
          @users_for_membership = @current_user.company.whole_company_users.map { |u| [u.whole_name, u.id] }
          @users_for_admin = []
          @members = []
          @admins = []

          render template: '/backoffice_core/groups/new', alert: 'Skupinu se nepodařilo vytvořit.'
        end
      else
        redirect_to new_group_url
      end
    end
  
    def edit
      @group = Group.find(params[:id])
      @companies_for_select = @current_user.company.tree.one_level_array.map { |c| [c.title, c.id] }
      @users_for_membership = @current_user.company.whole_company_users.map { |u| [u.whole_name, u.id] }
      @members = @group.members.map { |m| [m.whole_name, m.id] }
      @admins = @group.admins.map { |a| [a.whole_name, a.id] }
      @users_for_admin = []
      @members.each { |m| @users_for_admin << m unless @group.is_admin?(User.find(m[1])) or @group.is_main_admin?(User.find(m[1])) }
      authorize! :edit, @group
    end
    
    # pri ulkadani se neberou v potaz nove pridani uzivatele
    # podezreni na nulove (neodeslane) selectboxy
    def update
      if params[:group] and params[:group][:title].length > 0
        @group = Group.find(params[:id])

        if @group.update_attributes(params[:group])
          if can? :add_members, @group # admin_add_user or is_main_admin?
            if params[:admins].present? and can? :add_admins, @group
              params[:admins].each do |a|
                user = User.find(a)
                @group.add_admin(user) unless @group.is_admin? user
              end
            end
            
            if params[:members].present?
              params[:members].each do |m|
                user = User.find(m)
                @group.add_member(user) unless @group.is_member? user
              end
            end
          end
          
          # smazeme uzivatele, kteri uz nejsou vybrani, adminy zmenime na uzivatele
          if can? :remove_admins, @group
            @group.remove_complementary_admins(params[:admins])
          end
          if can? :remove_members, @group
            @group.remove_complementary_members(params[:members])
          end

          # dame hlasku, pokud se user snazi pridavat/mazat uzivatele a nema opravneni          
          additional_members = (params[:members].is_a? Array) ? params[:members].size > @group.members_ids.size : false
          members_differ = (params[:members] != @group.members_ids)
          
          flash[:error] = []
          if cannot? :remove_members, @group and cannot? :add_members?, @group and members_differ
            flash[:error] << "Nemáte oprávnění měnit uživatele skupiny."
          elsif cannot? :remove_members, @group and members_differ
            flash[:error] << "Nemáte oprávnění mazat uživatele skupiny."
          elsif cannot? :add_members, @group and additional_members
            flash[:error] << "Nemáte oprávnění přidávat uživatele skupiny."
          end
          
          Log.create(title: 'Uživatel upravil nastavení skupiny ' + @group.title)
          redirect_to groups_url, notice: 'Skupina je uložena.'
        else
          render template: '/backoffice_core/groups/edit', alert: 'Skupinu se nepodařilo uložit.'
        end
      else
        @group = Group.new
        render template: '/backoffice_core/groups/edit', alert: 'Zadejte název skupiny'
      end
      authorize! :update, @group
    end
    
    def destroy
      group = Group.find(params[:id])
      deleted_name = group.title
      
      authorize! :destroy, group
      
      if group.destroy
        redirect_to groups_url, notice: "Skupina #{deleted_name} je smazaná." 
      else
        redirect_to groups_url, notice: "Skupina neni smazana." 
      end
    end
  end
end
