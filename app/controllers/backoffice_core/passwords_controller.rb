#!/bin/env ruby
# encoding: utf-8

require_dependency "backoffice_core/application_controller"

module BackofficeCore
  class PasswordsController < ApplicationController
    skip_before_filter :authorize
  
    def request_password
      render template: 'backoffice_core/passwords/request_password'
    end
    
    def set_token
      @user = User.find_by_email(params[:email])
      @errors = {}
      
      if @user
        token = Digest::SHA1.hexdigest(@user.email.to_s + Time.now.to_s)
        
        if @user.update_attribute(:renew_password_token, token)
          PasswordMailer.send_renew_password_token(token).deliver
          redirect_to request_password_url, notice: 'Během chvíle Vám přijde e-mail s odkazem pro nastavení nového hesla.'
        end
      elsif params[:email].length == 0
        @errors = { email: 'Zadejte e-mail, ke kterému je Váš účet registrovaný.' }
        render template: 'backoffice_core/passwords/request_password'
      else
        @errors = { email: 'K zadanému e-mailu není registrovaný žádný účet.'}
        render template: 'backoffice_core/passwords/request_password'
      end
    end
    
    def new
      @user = User.find_by_renew_password_token(params[:token])
      
      if @user
        render template: 'backoffice_core/passwords/new'
      elsif @user.nil?
        redirect_to request_password_url, alert: 'Zadaný odkaz již neplatí. Požádejte o nové heslo znovu.'
      end
    end
    
    def create
      @user = User.find_by_renew_password_token(params[:user][:token])
      @errors = {}
      
      if params[:user][:password].length == 0
        @errors = { password: 'Heslo nesmí být prázdné.' }
        render template: 'backoffice_core/passwords/new'
      elsif params[:user][:password] != params[:user][:password_confirmation]
        @errors = { password: 'Zadaná hesla se neshodují' }
        render template: 'backoffice_core/passwords/new'
      elsif @user.nil?
        redirect_to request_password_url, alert: 'K zadanému e-mailu není registrovaný žádný účet.'
      elsif @user.update_attribute(:password, params[:user][:password])
        @user.unset_token # forgot token when password changed
        redirect_to sign_in_url, notice: 'Vaše nové heslo bylo uloženo, nyní se můžete přihlásit.'
      end
    end
  end
end
