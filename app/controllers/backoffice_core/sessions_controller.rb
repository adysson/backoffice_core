#!/bin/env ruby
# encoding: utf-8

require_dependency "backoffice_core/application_controller"

module BackofficeCore
  class SessionsController < ApplicationController
    skip_before_filter :authorize, only: [:create, :new]
    
    def create
      @user = User.find_by_email(params[:user][:email])
      
      respond_to do |format|
        if @user.nil?
          format.html { redirect_to root_url, alert: "Špatný e-mail nebo heslo." }
        elsif @user.active? and @user.authenticate(params[:user][:password])
          session[:user_id] = @user.id
          @user.logs.create(title: 'Uživatel se přihlásil')

          # prvni prihlaseni firemniho uzivatele (ktery nebyl registrovany sefem)
          if @user.user_registration_id == 2 and @user.last_logged.nil? and @user.company.nil?
            @user.logged_now
            format.html { redirect_to company_structure_url, notice: 'Nyní nastavte Vaši společnost.' }
          # prvni prihlaseni uzivatele registrovaneho sefem -> zmenit prednastavene heslo
          elsif @user.registered_by_chief? and @user.last_logged.nil?
            @user.logged_now
            format.html { redirect_to change_password_path, notice: 'Nyní si prosím změňte Vaše heslo pro přihlášení do systému Backoffice.' }
          # prvni prihlaseni nezakladatele firmy - vyplni profil
          elsif @user.last_logged.nil?
            @user.logged_now
            format.html { redirect_to edit_user_url(@user), notice: 'Nyní prosím vyplňte svůj uživatelský profil. Budete moci využívat více funkcí.' }
          end
          @user.logged_now
          format.html { redirect_to settings_url }
        else
          format.html { redirect_to root_url, alert: "Špatný e-mail nebo heslo." }
        end
      end
    end
  
    def new
      if session[:user_id]
        redirect_to settings_url
      else
        @user = User.new
        render template: 'backoffice_core/sessions/new'
      end
    end
    
    def destroy
      session[:user_id] = nil
      redirect_to root_url, alert: 'Uživatel odhlášen. Brzy na viděnou.'
    end
  end
end
