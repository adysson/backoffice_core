#!/bin/env ruby
# encoding: utf-8

require_dependency "backoffice_core/application_controller"

module BackofficeCore
  class SettingsController < ApplicationController
    def index
      @roles = Role.all
      @users = User.all
      @companies_for_select = Company.all.map { |c| [c.title, c.id] }
    end
  end
end
