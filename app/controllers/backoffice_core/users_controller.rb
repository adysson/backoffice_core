#!/bin/env ruby
# encoding: utf-8

require_dependency "backoffice_core/application_controller"

module BackofficeCore
  class UsersController < ApplicationController
    skip_before_filter :authorize, only: [:new, :create, :activate]
    skip_load_and_authorize_resource class: BackofficeCore::User, only: :new

    def index
    end
  
    def create
      @user = User.new(params[:user])
      
      respond_to do |format|
        if @user.save
          # log the signing up, send activation mail and redirect as success
          Log.create(user_id: @user.id, title: 'Uživatel se zaregistroval, typ profilu: ' + @user.user_registration.title + '.')
          UserMailer.activate_user(@user).deliver

          format.html { redirect_to root_url, notice: 'Zkontrolujte svůj email a potvrďte Vaši registraci. Tuto stránku můžete zavřít.' }
        else
          @user_registrations = UserRegistration.all.map { |ur| [ur.id, ur.title] }

          format.html { render 'backoffice_core/users/new', alert: 'Nepodařilo se uložit uživatele.' }
        end
      end
    end
  
    def new
      @user = User.new
      @user_registrations = UserRegistration.all.map { |ur| [ur.id, ur.title] }
      render template: 'backoffice_core/users/new'
    end
  
    def update
      @user = @current_user
      
      if @user.update_attributes(params[:user])
        @user.logs.create(title: 'Uživatel změnil nastavení svého profilu.')
        redirect_to edit_user_url(@user), notice: 'Uživatel uložen.'
      else
        @user_editation = true # starts city field completion
        @contact_phones = @user.contact_phones.map { |cp| [cp.phone, cp.id] }
        @contact_emails = @user.contact_emails.map { |ce| [ce.email, ce.id] }
        @cities = City.all.map { |c| c.title }
        @countries = Country.all.map { |c| [c.title, c.id] }
        @employee_posts = @user.company.employee_posts.map { |ep| [ep.title, ep.id] } # load employee posts of the company
        @nationalities = Nationality.all.map { |n| [n.title, n.id] }
        @languages = Language.all
        @logs = @user.logs.map { |l| [l.created_at.to_s + ' ' + l.title, l.id] }

        render template: 'backoffice_core/users/edit'
      end
    end
  
    def edit
      @user_editation = true # starts city field completion
      @user = @current_user
      @user.birth_date = @user.birth_date.strftime("%d. %m. %Y") unless @user.birth_date.nil?
      @cities = City.all.map { |c| c.title }
      @contact_phones = @user.contact_phones.order('primary_phone DESC, phone').map { |cp| [cp.phone, cp.id] }
      @contact_emails = @user.contact_emails.order('primary_email DESC, email').map { |ce| [ce.email, ce.id] }
      @countries = Country.all.map { |c| [c.title, c.id] }
      if @user.company.nil?
        @employee_posts = []
      else
        @employee_posts = @user.company.top_parent_company.employee_posts.map { |ep| [ep.title, ep.id] }  # load employee posts of the company
      end
      @nationalities = Nationality.all.map { |n| [n.title, n.id] }
      @languages = Language.all
      @logs = @user.logs.order('created_at DESC').map { |l| [l.to_s, l.id] }

      # prepares new contact phone form
      @contact_phone = ContactPhone.new
      @phone_types = self.column_values_to_a('backoffice_core_contact_phones', 'phone_type')
      @phone_usages = self.column_values_to_a('backoffice_core_contact_phones', 'phone_usage')

      # prepares new contact email form
      @contact_email = ContactEmail.new
      @email_usages = self.column_values_to_a('backoffice_core_contact_emails', 'email_usage')
    end
  
    def destroy
      u = User.find(params[:id])
      authorize! :destroy, u
      
      if u.destroy
        UserMailer.user_deleted(u).deliver
        redirect_to :back, notice: 'Uživatel byl smazán.'
      else
        if u.errors.any?
          notice = u.errors[:base]
        else
          notice = 'Uživatel nesmazán.'
        end
        redirect_to :back, notice: notice
      end
    end
    
    def activate
      @user = User.find_by_activation_code(params[:code])
      
      if @user.nil?
        redirect_to root_url, alert: 'Aktivace uživatele se nepodařila, tento aktivační kód neexistuje.'
      elsif @user.update_attributes({ activation_code: nil, active: 1 })
        @user.logs.create(title: 'Uživatel aktivoval svůj účet.')
        UserMailer.user_created(@user).deliver
        redirect_to root_url, alert: 'Váš uživatelský účet je aktivní, nyní se můžete přihlásit.'
      end
    end
    
    def change_password

    end
    
    def change_password_update
      if params[:user] and params[:user][:password].length > 0
        if @current_user.update_attributes(params[:user])
          @current_user.logs.create(title: 'Uživatel si změnil heslo.')
          redirect_to settings_url, notice: 'Vaše nové heslo bylo uloženo.'
        else
          @current_user.logs.create(title: 'Chyba při pokusu o uložení nového hesla.')
          render template: '/backoffice_core/users/change_password', alert: 'Heslo se nepodařilo uložit, zkuste to znovu.'
        end
      else
        redirect_to change_password_url, notice: 'Vyplňte nové heslo.'
      end
    end
    
    def forward_roles
      @users = User.where("id <> " + @current_user.id.to_s).map { |u| [u.whole_name, u.id] }
      @users.delete @current_user      
    end
    
    def forward_roles_update
      @selected_user = User.find(params[:selected_user_id])

      @current_user.roles.each do |r|
        unless @selected_user.roles.all.include? r
          @selected_user.roles << r
        end
      end
      redirect_to forward_roles_url, notice: 'Váš kolega ' + @selected_user.whole_name + ' má nyní všechny Vaše práva pro práci se systémem.'
    end
    
    # automaticke vypsani moznych hodnot pro selectbox z MySQL sloupce typu ENUM
    # bylo by dobre presunout na vhodnejsi misto
    def column_values_to_a(table_name, column_name)
      column_values = ActiveRecord::Base.connection.execute('SELECT column_type 
                                                                  FROM information_schema.columns 
                                                                  WHERE table_name = "' + table_name + '" 
                                                                    AND column_name = "' + column_name + '";')
      column_values = column_values.to_a[0].to_s
      slice_start = column_values.index('(') + 2
      slice_end = column_values.length - 5
      
      column_values[slice_start..slice_end].split("','")
    end
  end
end
