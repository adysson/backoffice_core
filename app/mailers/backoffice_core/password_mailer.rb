#!/bin/env ruby
# encoding: utf-8

module BackofficeCore
  class PasswordMailer < ActionMailer::Base
    default from: "backoffice_core@backoffice.cz"
    
    def send_renew_password_token(token)
      @user = User.find_by_renew_password_token(token)
      @url = password_token_url(token)
      mail(to: @user.email, subject: 'Změna hesla Vašeho uživatelského účtu')
    end
  end
end
