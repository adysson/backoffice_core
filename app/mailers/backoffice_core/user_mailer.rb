#!/bin/env ruby
# encoding: utf-8

module BackofficeCore
  class UserMailer < ActionMailer::Base
    default from: "backoffice_core@backoffice.cz"
    
    def activate_user(user)
      @user = user
      mail(to: @user.email, subject: 'Aktivujte svůj účet v Backoffice')
    end
    
    def registered_by_chief(user, password)
      @user = user
      @password = password
      mail(to: @user.email, subject: 'Váš nový účet v Backoffice')
    end
    
    def user_created(user)
      @user = user
      mail(to: @user.email, subject: 'Účet v Backoffice je aktivní')
    end

    def user_deleted(user)
      @user = user
      mail(to: @user.email, subject: 'Účet v Backoffice zrušen')
    end
  end
end
