#!/bin/env ruby
# encoding: utf-8

module BackofficeCore
  class Ability
    include CanCan::Ability
    
    def initialize(user)
      if user.role? :manage_users
        can :read, User
        can :update, User
      end
      if user.role? :delete_users
        can :destroy, User
      end
      if user.role? :company_founder
        can :manage, Company, id: user.company.tree.ids
      end

      # admins can update group      
      can [:update, :edit], Group, admins: { id: user.id }      
      can :destroy, Group, admin_destroy_group: true, admins: { id: user.id }
      can :add_members, Group, admin_add_user: true, admins: { id: user.id }
      can :remove_members, Group, admin_remove_user: true, admins: { id: user.id }
      # group main admin can do everything
      can [:manage, :add_members, :remove_members, :add_admins, :remove_admins], Group, main_admin_user_id: user.id      
#      can :add_members, Group, main_admin_user_id: user.id
#      can :remove_members, Group, main_admin_user_id: user.id
#      can :remove_admins, Group, main_admin_user_id: user.id
#      can :add_admins, Group, main_admin_user_id: user.id
    end
  end
end
