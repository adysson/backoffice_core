#!/bin/env ruby
# encoding: utf-8

module BackofficeCore
  class Company < ActiveRecord::Base
    attr_accessible :id, :parent_company_id, :title, :ico, :dic,
                    :street, :orientation_number, :house_number, :city, :postal_code,
                    :phone, :cell_phone, :fax, :email, :www,
                    :fb, :icq, :skype, :description, :company_type_id,
                    :country_id, :parent_company_id
                    
    validates_presence_of :title, on: :update, message: "nesmí být prázdný"
    
    belongs_to :company_type
    belongs_to :country
    belongs_to :parent_company, class_name: "Company", foreign_key: "parent_company_id"
    has_many :children_companies, class_name: "Company", foreign_key: "parent_company_id"
    has_many :company_detail_types
    has_many :company_details
    has_many :employee_posts
    has_many :users
    has_one :group, foreign_key: "automatic_company_id"

    # counts bill for one company    
    def bill_for_all_company_licenses
      bill = 0
      
      License.all.each do |l|
        bill += self.count_company_licenses(l) * l.price
      end
      bill
    end
    
    # counts complete bill for top company and subcompanies
    def bill_for_whole_company_licenses
      bill = 0
      
      self.tree.one_level_array.each do |c|
        bill += c.bill_for_all_company_licenses
      end
      bill
    end
    
    def branch_office_title
      if self.is_top_parent?
        "Mateřská kancelář"
      else
        self.title
      end
    end    
    
    def children_companies_hash(parent_company)
      hash = {}
      
      if parent_company.children_companies.any?
        parent_company.children_companies.each do |child|
          hash[child.to_s] = self.children_companies_hash(child)
        end
      end
      hash
    end
    
    def count_all_subcompanies
      self.tree.one_level_array.size - 1
    end

    def count_company_licenses(license)
      counter = 0
      
      self.users.each do |u|
        counter += 1 if u.license == license
      end
      counter
    end
    
    def count_indirect_subcompanies
      self.count_all_subcompanies - self.children_companies.count
    end
    
    def count_paid_users_licenses
      licenses_count = 0
      
      self.users.each do |u|
        licenses_count += 1 if u.license.price > 0
      end
      licenses_count
    end

    def count_whole_company_users
      count = 0
      
      self.tree.one_level_array.each do |i|
        count += i.users.count
      end
      count
    end
    
    def count_company_founders
      count = 0
      
      self.users.each do |u|
        count += 1 if u.role?('company_founder')
      end
      count
    end

    def is_child_of?(company)
      ct = CompanyTree.new(company)        
      ct.ids.include? self.id
    end
    
    def is_top_parent?
      self.parent_company.blank?
    end
    
    def primary_post
      self.employee_posts.where("primary_post = 1").first || self.top_parent_company.employee_posts.where("primary_post = 1").first || self.top_parent_company.employee_posts.first
    end
    
    # company_details: key, value = company_detail.id, detail_value
    # recursive: array of company_detail.ids (from first param)
    #            that should be saved in all subcompanies
    def save_existing_company_details(company_details, recursive)
      company_details.each do |key, value|
        # if recursive, pass to all subcompanies
        if recursive.is_a? Hash and recursive.key? key
          detail_title = self.company_details.find(key).company_detail_type.title
          
          self.tree.one_level_array.each do |company|
            cdt = company.company_detail_types.find_by_title(detail_title)
            
            # if chosen detail type doesn't exist, create it and create it's value
            if cdt.blank?
              cdt = company.company_detail_types.create(title: detail_title)
              cdt.company_details.create(detail_value: value, company_id: company.id)
            else # cdt existuje, smazeme dosavadni cd a nastavime nove
              cd = company.company_details.where(company_detail_type_id: cdt.id).first
              
              if cd.blank?
                cdt.company_details.create(detail_value: value, company_id: company.id)
              else
                cd.update_attribute('detail_value', value)
              end
            end
          end
        else
          cd = self.company_details.find(key)
          cd.update_attribute('detail_value', value)
        end
      end
    end

    # saves existing details only in chosen company, not into subcompanies
    def save_existing_company_details_only_node(company_details)
      company_details.each do |key, value|
        cd = self.company_details.find(key)
        cd.update_attribute('detail_value', value)
      end
    end

    # nepropise se novy detail do podpobocek
    def save_new_company_details(new_company_details, recursive)
      x = {}
      new_company_details.each do |key, detail|
          cdt = self.company_detail_types.find_by_title(detail[:title])

          if cdt.blank?
            cdt = self.company_detail_types.create(title: detail[:title])
          end
          
          cd = self.company_details.create(company_detail_type_id: cdt.id, detail_value: detail[:detail_value])
          
          if recursive.is_a? Hash and recursive.key? key
            self.save_existing_company_details({ cd.id => cd.detail_value },
                                               { cd.id => "1" })
          end
          
      end
    end
    
    def save_params_to_all_subcompanies(params, company_params)
      if params.present? and params.any? and self.tree.one_level_array.any?
        params.each do |param, value|
          self.tree.one_level_array.each do |company|
            # rozdil mezi company[:title] a company['title'] - ten druhy vytvarim, potrebuju ale ten druhy
            raise ActiveRecord::RecordInvalid unless company.update_attribute(param, company_params[param])
          end
        end
      end
    end
    
    def to_s
      self.title
    end

    def top_parent_company
      if self.parent_company.blank?
        self
      else
        self.find_top_parent_company(self.parent_company)      
      end
    end
    
    def tree
      CompanyTree.new(self)
    end
    
    def whole_company_users
      users = []
      
      self.tree.one_level_array.each do |company|
        company.users.each do |u|
          users << u
        end
      end
      users
    end
    
    def whole_company_users_count_by_posts
      posts = []
      
      self.employee_posts.each do |post|
        posts << [post.title, post.users.count]
      end
      posts
    end
    
    def manage_automatic_group(main_admin_user)
      if self.group.blank?
        automatic_group = Group.create(title: self.title, 
                            description: 'automatická skupina společnosti', 
                            main_admin_user_id: main_admin_user.id.to_s, 
                            automatic: 1, 
                            automatic_company_id: self.id.to_s, 
                            created_at: Time.now)
      else
        automatic_group = self.group
        automatic_group.update_attribute('title', self.title)
      end
      automatic_group.save!
    end   

    protected

    def find_top_parent_company(company)
      if company.parent_company.present?
        self.find_top_parent_company(company.parent_company)
      else
        company
      end
    end 
  end
end
