module BackofficeCore
  class CompanyDetail < ActiveRecord::Base
    attr_accessible :id, :company_detail_type_id, :company_id, :detail_value
    
    validates_presence_of :company_detail_type_id
    
    belongs_to :company
    belongs_to :company_detail_type
    
    def to_s
      self.id.to_s + ': ' + self.detail_value
    end
  end
end
