module BackofficeCore
  class CompanyDetailType < ActiveRecord::Base
    attr_accessible :title, :company_id, :id
    
    validates_presence_of :title
    validates_uniqueness_of :title, scope: :company_id
    
    belongs_to :company
    has_many :company_details
  end
end
