#!/bin/env ruby
# encoding: utf-8

module BackofficeCore
  class CompanyTree
    include ActiveModel::Validations
    include ActiveModel::Conversion
    extend ActiveModel::Naming
    
    attr_accessor :company, :ids, :one_level_array
    
    def initialize(company)
      if company.is_a? Company
        self.company = company
      elsif company.is_a? Integer
        self.company = Company.find(company)
      end
      self.one_level_array = []
      self.one_level_array << self.company
      self.ids = []
      self.ids << self.company.id
      self.list(self.company)
    end
    
    def list(parent_company)
      if parent_company.children_companies.any?        
        parent_company.children_companies.each do |child|
          self.one_level_array << child
          self.ids << child.id
          self.list(child)
        end
      end
    end
  end
end
