module BackofficeCore
  class CompanyType < ActiveRecord::Base
    attr_accessible :title
    
    has_many :companies
  end
end
