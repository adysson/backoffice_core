#!/bin/env ruby
# encoding: utf-8

module BackofficeCore
  class ContactEmail < ActiveRecord::Base
    attr_accessible :id, :email, :email_usage, :note, :primary_email, :user_id
    
    validates_uniqueness_of :email, scope: :user_id, message: 'Tento email již je v databázi uložen.'
    
    belongs_to :user
    
    def to_s
      self.email
    end
  end
end
