#!/bin/env ruby
# encoding: utf-8

module BackofficeCore
  class ContactPhone < ActiveRecord::Base
    attr_accessible :phone, :phone_type, :phone_usage, :note, :primary_phone, :user_id
    
    validates_uniqueness_of :phone, scope: :user_id, message: 'Tento telefon již je v databázi uložen'
    
    belongs_to :user
    
    def to_s
      self.phone
    end
  end
end
