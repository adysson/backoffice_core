module BackofficeCore
  class Country < ActiveRecord::Base
    attr_accessible :title
    
    has_many :users
    has_many :companies
  end
end
