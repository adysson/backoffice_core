module BackofficeCore
  class EmployeePost < ActiveRecord::Base
    attr_accessible :company_id, :title, :primary_post
    
    has_many :users
    belongs_to :company
  end
end
