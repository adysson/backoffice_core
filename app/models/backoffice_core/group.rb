#!/bin/env ruby
# encoding: utf-8

module BackofficeCore
  class Group < ActiveRecord::Base  
    attr_accessible :admin_add_user, :admin_destroy_group, :admin_remove_user, :automatic, 
                    :automatic_company_id, :created_at, :description, :id, 
                    :main_admin_user_id, :title, :updated_at

    has_and_belongs_to_many :admins, join_table: :backoffice_core_groups_users, class_name: User, conditions: ["backoffice_core_groups_users.admin = 1"]#{ admin: 1 }
    has_and_belongs_to_many :members, join_table: :backoffice_core_groups_users, class_name: User
    belongs_to :company, foreign_key: "automatic_company_id"
    belongs_to :main_admin, foreign_key: "main_admin_user_id", class_name: User
    
    validates_presence_of :title, message: "nesmí být prázdný"
    
    def automatic?
      self.automatic == 1 ? true : false
    end

#    def main_admin
#      if self.main_admin_user_id
#        User.find(self.main_admin_user_id)
#      end
#    end
    
    def is_main_admin? user
      user == self.main_admin
    end
   
    def add_member(user, admin = nil)
      if user.is_a? User
        if !self.is_member? user
          sql = 'INSERT INTO backoffice_core_groups_users
                 SET group_id = ' + self.id.to_s + ',
                     user_id = ' + user.id.to_s + ',
                     created_at = NOW()'
          if admin.present?
            sql += ', admin = 1'
          end
        elsif self.is_member? user and admin.present?
          sql = 'UPDATE backoffice_core_groups_users
                 SET admin = 1,
                     updated_at = NOW()
                 WHERE group_id = ' + self.id.to_s + '
                 AND user_id = ' + user.id.to_s
        elsif self.is_admin? user and admin.blank?
          sql = 'UPDATE backoffice_core_groups_users
                 SET admin = NULL,
                     updated_at = NOW()
                 WHERE group_id = ' + self.id.to_s + '
                 AND user_id = ' + user.id.to_s
        end
      else
        raise TypeError, "Nespravny format User"
      end
      
      if sql
        ActiveRecord::Base.connection.insert(sql)
      else
        false
      end
    end
    
    def add_admin(user)
        self.add_member(user, 1)
    end
    
    def remove_member(user)
      if user.is_a? User
        self.members.delete(user)
      else
        raise TypeError, "Nespravny format User"
      end
    end
    
    def remove_complementary_members(users_ids, admin = nil)
      users = []
      complementary = []
      
      if users_ids.is_a? Array
        users_ids.each do |user_id|
          users << User.find(user_id)
        end
      end
      
      if admin
        complementary = self.admins - users
        
        if complementary.size > 0
          complementary.each do |ca|
            self.add_member(ca) # change admin to member
          end
        end
      else     
        complementary = self.members - users
        
        if complementary.size > 0
          complementary.each do |cu|
            self.remove_member(cu)
          end
        end
      end
    end
    
    def remove_complementary_admins(users_ids)
      self.remove_complementary_members(users_ids, 1)
    end
    
    def is_member?(user)
      if user.is_a? User
        self.members.include?(user)
      else
        raise TypeError, "Nespravny format User"
      end
    end
    
    def last_member
      a = self.members.order("created_at DESC").first
      a if a.present?
    end
    
    def is_admin?(user)
      if user.is_a? User
        self.admins.include?(user)
      else
        raise TypeError, "Nespravny format User"
      end
    end
    
    def admins_ids
      self.admins.map { |a| a.id.to_s }
    end
    
    def members_ids
      self.members.map { |m| m.id.to_s }
    end
  end
end
