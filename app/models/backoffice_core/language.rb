module BackofficeCore
  class Language < ActiveRecord::Base
    attr_accessible :title
    
    has_and_belongs_to_many :users, join_table: :backoffice_core_languages_users
  end
end
