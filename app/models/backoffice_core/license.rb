module BackofficeCore
  class License < ActiveRecord::Base
    attr_accessible :id, :duration, :price, :title, :license_type
    
    has_many :users
    has_and_belongs_to_many :roles, join_table: :backoffice_core_licenses_roles
    
    def to_s
      '<License: ' + self.license_type + ' (license_id: ' + self.id.to_s + ')>'
    end
    
    def paid_licenses
      self.where("price > 0")
    end
  end
end
