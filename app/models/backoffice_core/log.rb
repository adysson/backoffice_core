module BackofficeCore
  class Log < ActiveRecord::Base
    attr_accessible :description, :title, :user_id, :created_at
    
    belongs_to :user
    
    def to_s
      self.created_at.strftime("%d. %-m. %Y %k:%M:%S").to_s + ' ' + title
    end
  end
end
