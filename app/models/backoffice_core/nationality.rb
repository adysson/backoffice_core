module BackofficeCore
  class Nationality < ActiveRecord::Base
    attr_accessible :title
    
    has_many :users
  end
end
