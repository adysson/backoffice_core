module BackofficeCore
  class Role < ActiveRecord::Base
    attr_accessible :id, :name, :role_type
    attr_accessible :license_ids
    
    has_and_belongs_to_many :users, join_table: :backoffice_core_roles_users
    has_and_belongs_to_many :licenses, join_table: :backoffice_core_licenses_roles
    # k dane roli musi mit vyplnene urcite udaje
  end
end
