#!/bin/env ruby
# encoding: utf-8

module BackofficeCore
  class User < ActiveRecord::Base
    before_create :set_activation_code
    after_create :set_default_license, :set_primary_contact_email
    before_destroy :is_not_company_founder?
  
    attr_accessible :activation_code, :active, :birth_date, :children_count,
                    :city, :company_id, :country_id, :degree_after, 
                    :degree_before, :description, :dic, :email, 
                    :employee_post_id, :evidence_number, :fb, :has_children,
                    :has_partner, :has_pet, :has_wife, :house_number,
                    :ico, :icq, :id, :language_ids, :last_logged, :license_expiration, :license_id,
                    :motto, :name, :nationality_id, :orientation_number,
                    :password, :password_confirmation, :pet_race, :postal_code, :registered_by_chief,
                    :skype, :street, :surname, :twitter, :user_registration_id, :www,
                    :show_degree_before, :show_degree_after, :show_evidence_number, :show_company,
                    :show_branch_office, :show_address, :show_motto, :show_description
    
    validates_presence_of :email, on: :create, message: "nesmí být prázdný"
    validates_uniqueness_of :email, on: :create, message: "již je zaregistrován"
    validates_presence_of :password, on: :create, message: "nesmí být prázdné"
    validates :password, confirmation: { on: :create, message: "zadaná hesla se neshodují" }
    validates :password_confirmation, presence: { on: :create, attr: nil, message: "nesmí být prázdné" }
    validates_presence_of :user_registration_id, message: "zadejte typ registrace"
    
    has_secure_password
    
    belongs_to :company
    belongs_to :countries
    belongs_to :employee_post
    belongs_to :license
    belongs_to :nationality
    belongs_to :user_registration
    has_and_belongs_to_many :groups, join_table: :backoffice_core_groups_users
    has_and_belongs_to_many :languages, join_table: :backoffice_core_languages_users
    has_and_belongs_to_many :roles, join_table: :backoffice_core_roles_users
    has_many :contact_phones
    has_many :contact_emails
    has_many :logs
    
    def active?
      self.active
    end

    # checks user's ability
    def role?(role_type)
      self.roles.include?(Role.find_by_role_type(role_type))
    end
    
    def unset_token
      self.update_attribute(:renew_password_token, nil)
    end

    def logged_now
      self.update_attribute(:last_logged, Time.now.to_s(:db))
    end

    def create_default_company # and give role to the founder
      if self.company.blank?
        company = Company.create
        self.company = company
        self.save!
        
        r = Role.find_by_role_type('company_founder')
        unless self.roles.all.include? r
          self.roles.push r
        end
      end
    end

    def primary_contact_phone
      self.contact_phones.where("primary_phone = 1").first || self.contact_phones.first
    end
    
    def register_by_chief
      self.password = Array.new(8){rand(36).to_s(36)}.join
      self.password_confirmation = self.password
      self.user_registration_id = 2
      self.registered_by_chief = 1
      self.save!
      UserMailer.registered_by_chief(self, self.password).deliver
    end
    
    def registered_by_chief?
      self.registered_by_chief
    end
      
    def whole_name
      if self.name.present? and self.surname.present?
        self.name + ' ' + self.surname
      elsif self.name.present?
        self.name
      elsif self.surname.present?
        self.surname
      else
        self.email
      end
    end
    
    protected
    
    def generate_random_password
	    (0...8).map{65.+(rand(26)).chr}.join
    end
    
    # used on user creation
    def set_activation_code
      self.activation_code = Digest::SHA1.hexdigest(Time.now.to_s+self.email)
    end
    
    def set_license(license)
      if self.license_id.blank? # tuto podminku presunout do nastaveni after_create
        self.license_id = license.id
        
        if license.duration == 'year'
          self.license_expiration = 1.years.from_now
        elsif license.duration == 'month'
          self.license_expiration = 1.months.from_now
        end
        self.save!
      end
      self.set_roles_on_reference_to_license
    end
    
    # sets default license after account registration
    def set_default_license
      default_license = License.find_by_license_type('demo')
      self.set_license(default_license)
    end
    
    def set_roles_on_reference_to_license
      # delete all automatic roles of this user (defined by previous license)
      ActiveRecord::Base.connection.execute('DELETE FROM backoffice_core_roles_users 
                                               WHERE user_id = ' + self.id.to_s + '
                                               AND automatic = 1')

      # automatically adds roles defined by actual license
      license = License.find_by_id(self.license_id)
      license.roles.each { |r| ActiveRecord::Base.connection.execute('INSERT INTO backoffice_core_roles_users (user_id, role_id, automatic) VALUES (' + self.id.to_s + ', ' + r.id.to_s + ', 1)') }
    end
        
    def set_primary_contact_email
      self.contact_emails.create(email: self.email, primary_email: 1)
    end
    
    def is_not_company_founder?
      if self.role?('company_founder') and self.company.count_company_founders == 1
        errors.add(:base, "Nemůžete smazat jediného zakladatele firmy. Uživatel musí prvně svá práva předat některému z kolegů.")
        false
      else
        true
      end
    end
  end
end
