module BackofficeCore
  class UserRegistration < ActiveRecord::Base
    attr_accessible :id, :title
    
    has_many :users
  end
end
