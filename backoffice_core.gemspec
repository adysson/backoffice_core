$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "backoffice_core/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "backoffice_core"
  s.version     = BackofficeCore::VERSION
  s.authors     = ["TODO: Your name"]
  s.email       = ["TODO: Your email"]
  s.homepage    = "TODO"
  s.summary     = "TODO: Summary of BackofficeCore."
  s.description = "TODO: Description of BackofficeCore."

  s.files = Dir["{app,config,db,lib}/**/*"] + ["MIT-LICENSE", "Rakefile", "README.rdoc"]
  s.test_files = Dir["test/**/*"]

  s.add_dependency "rails", "~> 3.2.12"
  # s.add_dependency "jquery-rails"

  s.add_development_dependency "mysql2"
end
