BackofficeCore::Engine.routes.draw do
  match 'select_cities/:city_title' => 'cities#select_cities', as: 'select_cities'
  
  resources :companies, only: [:show, :destroy]
  controller :companies do
    get 'company_structure' => :company_structure
    put 'company_structure' => :company_structure_update
    get 'company_details' => :company_details
    put 'company_details' => :company_details_update
    get 'company_employee_posts' => :company_employee_posts
    put 'company_employee_posts' => :company_employee_posts_update
    get 'company_users' => :company_users
    put 'company_users' => :company_users_update
    get 'company_finish' => :company_finish
    put 'company_finish' => :company_finish_update
  end
  match 'company_details/:company_id' => 'companies#company_details', as: 'specific_company_details'
  match 'company_users/:company_id' => 'companies#company_users', as: 'specific_company_users'
  match 'create_company/:parent_company_id' => 'companies#create', as: 'create_company'
  match 'return_company_users/:company_id' => 'companies#return_company_users', as: 'return_company_users'
  
  resource :contact_emails
  resource :contact_phones
  
  resources :groups, except: :show
  
  match 'employee_post/:id' => 'employee_posts#destroy', as: 'employee_post', via: :delete

  controller :passwords do
    get 'request_password' => :request_password
    put 'password' => :set_token
    post 'password' => :create
  end
  match 'password/:token' => 'passwords#new', as: 'password_token'
  
  controller :settings do
    get 'settings' => :index
  end
  
  controller :sessions do
    get 'sign_in' => :new
    post 'sign_in' => :create
    delete 'sign_out' => :destroy
  end

  resources :users
  controller :users do
    put 'forward_roles' => :forward_roles_update
    get 'change_password' => :change_password
    put 'change_password' => :change_password_update
  end
  match 'activate_user/:code' => 'users#activate', as: 'activate_user'
  match 'forward_roles' => 'users#forward_roles', as: 'forward_roles'
  
  root to: "sessions#new"
end
