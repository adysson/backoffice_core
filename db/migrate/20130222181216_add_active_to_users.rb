class AddActiveToUsers < ActiveRecord::Migration
  def change
    add_column :backoffice_core_users, :active, :boolean, default: 0
  end
end
