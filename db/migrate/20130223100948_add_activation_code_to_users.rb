class AddActivationCodeToUsers < ActiveRecord::Migration
  def change
    add_column :backoffice_core_users, :activation_code, :string, limit: 40
  end
end
