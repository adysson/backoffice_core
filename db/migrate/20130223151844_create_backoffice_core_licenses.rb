#!/bin/env ruby
# encoding: utf-8

class CreateBackofficeCoreLicenses < ActiveRecord::Migration
  def change
    create_table :backoffice_core_licenses do |t|
      t.string :title, limit: 30
      t.string :license_type, limit: 5
      t.integer :price
      t.string :duration, default: 'year', limit: 5

      t.timestamps
    end
    execute "INSERT INTO backoffice_core_licenses SET title = 'Demo ucet', license_type = 'demo', price = 0, duration = 'year'"
  end
end
