class AddLicenseIdAndLicenseExpirationToUsers < ActiveRecord::Migration
  def up
    add_column :backoffice_core_users, :license_expiration, :date
    execute 'ALTER TABLE `backoffice_core_users` 
               ADD CONSTRAINT `fk_users_licenses` 
               FOREIGN KEY (`license_id`) REFERENCES `backoffice_core_licenses`(`id`)'  
  end
  
  def down
    execute 'ALTER TABLE `backoffice_core_users` 
               DROP FOREIGN KEY `fk_users_licenses`'
    execute 'ALTER TABLE `backoffice_core_users` 
               DROP INDEX `fk_users_licenses`'
    remove_column :backoffice_core_users, :license_expiration
  end
end
