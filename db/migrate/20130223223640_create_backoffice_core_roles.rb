class CreateBackofficeCoreRoles < ActiveRecord::Migration
  def change
    create_table :backoffice_core_roles do |t|
      t.string :name, limit: 50
      t.string :role_type, limit: 50

      t.timestamps
    end
  end
end
