class CreateUsersRoles < ActiveRecord::Migration
  def up
    create_table :backoffice_core_users_roles do |t|
      t.integer :user_id
      t.integer :role_id
      t.boolean :automatic, default: 0

      t.timestamps
    end
    execute 'ALTER TABLE `backoffice_core_users_roles`
               ADD CONSTRAINT `uk_users_roles` UNIQUE (`user_id`, `role_id`)'
    execute 'ALTER TABLE `backoffice_core_users_roles` 
               ADD CONSTRAINT `fk_users_roles_user` 
               FOREIGN KEY (`user_id`) REFERENCES `backoffice_core_users`(`id`) ON DELETE CASCADE ON UPDATE CASCADE'
    execute 'ALTER TABLE `backoffice_core_users_roles` 
               ADD CONSTRAINT `fk_users_roles_role` 
               FOREIGN KEY (`role_id`) REFERENCES `backoffice_core_roles`(`id`)'
  end
  
  def down
    execute 'ALTER TABLE `backoffice_core_users_roles` 
               DROP FOREIGN KEY `fk_users_roles_user`'
    execute 'ALTER TABLE `backoffice_core_users_roles` 
               DROP FOREIGN KEY `fk_users_roles_role`'
    drop_table :backoffice_core_users_roles
  end
end
