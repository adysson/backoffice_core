class CreateLicensesRoles < ActiveRecord::Migration
  def up
    create_table :backoffice_core_licenses_roles do |t|
      t.integer :license_id
      t.integer :role_id

      t.timestamps
    end
    execute 'ALTER TABLE `backoffice_core_licenses_roles`
               ADD CONSTRAINT `uk_licenses_roles` UNIQUE (`license_id`, `role_id`)'
    execute 'ALTER TABLE `backoffice_core_licenses_roles` 
               ADD CONSTRAINT `fk_licenses_roles_license` 
               FOREIGN KEY (`license_id`) REFERENCES `backoffice_core_licenses`(`id`)'
    execute 'ALTER TABLE `backoffice_core_licenses_roles` 
               ADD CONSTRAINT `fk_licenses_roles_role` 
               FOREIGN KEY (`role_id`) REFERENCES `backoffice_core_roles`(`id`)'
  end
  
  def down
    execute 'ALTER TABLE `backoffice_core_licenses_roles` 
               DROP FOREIGN KEY `fk_licenses_roles_license`'
    execute 'ALTER TABLE `backoffice_core_licenses_roles` 
               DROP FOREIGN KEY `fk_licenses_roles_role`'
    drop_table :backoffice_core_licenses_roles
  end
end
