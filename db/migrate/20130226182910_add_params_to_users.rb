class AddParamsToUsers < ActiveRecord::Migration
  def up
    add_column :backoffice_core_users, :evidence_number, :string
    add_index :backoffice_core_users, :evidence_number, unique: true
    create_table :backoffice_core_employee_posts do |t|
      t.integer :company_id
      t.string :title
    end
    add_column :backoffice_core_users, :employee_post_id, :integer
    execute 'ALTER TABLE `backoffice_core_users` 
               ADD CONSTRAINT `fk_users_employee_posts` 
               FOREIGN KEY (`employee_post_id`) REFERENCES `backoffice_core_employee_posts`(`id`)'
  end

  def down
    execute 'ALTER TABLE `backoffice_core_users` 
               DROP FOREIGN KEY `fk_users_employee_posts`'
    drop_table :backoffice_core_employee_posts
    remove_index :backoffice_core_users, column: :evidence_number
    remove_column :backoffice_core_users, :employee_post_id, :evidence_number
  end
end
