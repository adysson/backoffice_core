class AddContactPhonesAndEmails < ActiveRecord::Migration
  def up
    execute "CREATE TABLE `backoffice_core_contact_phones` (
        `id` int NOT NULL AUTO_INCREMENT PRIMARY KEY,
        `phone` varchar(18) NULL,
        `phone_type` enum('','Mobilni','Pevna linka','Internetovy','Satelitni','Zahranicni') NOT NULL,
        `phone_usage` enum('','Soukromy','Pracovni','Asistentka','Soukromy 2','Pracovni 2') NULL,
        `note` varchar(255) NULL,
        `primary` tinyint(1) NULL,
        `user_id` integer(11) NOT NULL
      ) COMMENT='' COLLATE 'utf8_general_ci';"
    execute 'ALTER TABLE `backoffice_core_contact_phones` 
               ADD CONSTRAINT `fk_contact_phones_user` 
               FOREIGN KEY (`user_id`) REFERENCES `backoffice_core_users`(`id`) ON DELETE CASCADE ON UPDATE CASCADE'
    # tabulky kontaktnich emailu
    execute "CREATE TABLE `backoffice_core_contact_emails` (
        `id` int NOT NULL AUTO_INCREMENT PRIMARY KEY,
        `email` varchar(150) NOT NULL,
        `email_usage` enum('','Soukromy','Pracovni','Asistentka','Soukromy 2','Pracovni 2') NULL,
        `note` varchar(255) NULL,
        `primary` tinyint(1) NULL,
        `user_id` integer(11) NOT NULL
      ) COMMENT='' COLLATE 'utf8_general_ci';"
    execute 'ALTER TABLE `backoffice_core_contact_emails` 
               ADD CONSTRAINT `fk_contact_emails_email` 
               FOREIGN KEY (`user_id`) REFERENCES `backoffice_core_users`(`id`) ON DELETE CASCADE ON UPDATE CASCADE'
  end

  def down
    execute 'ALTER TABLE `backoffice_core_contact_phones` 
               DROP FOREIGN KEY `fk_contact_phones_user`'
    drop_table :backoffice_core_contact_phones
    # tabulky kontaktnich emailu
    execute 'ALTER TABLE `backoffice_core_contact_emails` 
               DROP FOREIGN KEY `fk_contact_emails_email`'
    drop_table :backoffice_core_contact_emails
  end
end
