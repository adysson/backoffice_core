class AddCompanies < ActiveRecord::Migration
  def up
    create_table :backoffice_core_companies
    add_column :backoffice_core_users, :company_id, :integer
    execute 'ALTER TABLE `backoffice_core_users` 
               ADD CONSTRAINT `fk_companies_users` 
               FOREIGN KEY (`company_id`) REFERENCES `backoffice_core_companies`(`id`) ON DELETE SET NULL ON UPDATE CASCADE'
    # pridat foreign key pro employee_posts.company_id -> backoffice_core_companies.id
    execute "INSERT INTO `backoffice_core_companies` (`id`)
               VALUES (1);"
    execute "INSERT INTO `backoffice_core_employee_posts` (`company_id`, `title`)
               VALUES (1, 'Makler');"
    execute "INSERT INTO `backoffice_core_employee_posts` (`company_id`, `title`)
               VALUES (1, 'Asistentka');"
    execute "INSERT INTO `backoffice_core_employee_posts` (`company_id`, `title`)
               VALUES (1, 'Ucetni');"
    execute "INSERT INTO `backoffice_core_employee_posts` (`company_id`, `title`)
               VALUES (1, 'Manager');"
    execute "INSERT INTO `backoffice_core_employee_posts` (`company_id`, `title`)
               VALUES (1, 'Majitel');"
  end

  def down
    #execute 'ALTER TABLE `backoffice_core_employee_posts` 
    #           DROP FOREIGN KEY `fk_companies_employee_posts`'
    execute 'ALTER TABLE `backoffice_core_users` 
               DROP FOREIGN KEY `fk_companies_users`'
    remove_column :backoffice_core_users, :company_id
    drop_table :backoffice_core_companies
  end
end
