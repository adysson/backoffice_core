class CreateBackofficeCoreNationalities < ActiveRecord::Migration
  def up
    create_table :backoffice_core_nationalities do |t|
      t.string :title
    end
    execute 'INSERT INTO backoffice_core_nationalities (`title`) VALUES ("Ceska"),("Slovenska"),("Polska"),("Madarska")'
    execute 'ALTER TABLE `backoffice_core_users` 
               ADD CONSTRAINT `fk_nationalities_users` 
               FOREIGN KEY (`nationality_id`) REFERENCES `backoffice_core_nationalities`(`id`)'
  end
  
  def down
    execute 'ALTER TABLE `backoffice_core_users` 
               DROP FOREIGN KEY `fk_nationalities_users`'
    drop_table :backoffice_core_nationalities
  end
end
