class CreateBackofficeCoreCountries < ActiveRecord::Migration
  def up
    create_table :backoffice_core_countries do |t|
      t.string :title
    end
    execute 'INSERT INTO backoffice_core_countries (`title`) VALUES ("Ceska republika"),("Slovensko"),("Polsko"),("Madarsko")'
    execute 'ALTER TABLE `backoffice_core_users` 
               ADD CONSTRAINT `fk_countries_users` 
               FOREIGN KEY (`country_id`) REFERENCES `backoffice_core_countries`(`id`)'
  end
  
  def down
    execute 'ALTER TABLE `backoffice_core_users` 
               DROP FOREIGN KEY `fk_countries_users`'
    drop_table :backoffice_core_countries
  end

end
