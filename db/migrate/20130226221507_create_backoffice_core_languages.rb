class CreateBackofficeCoreLanguages < ActiveRecord::Migration
  def up
    create_table :backoffice_core_languages do |t|
      t.string :title
    end
    create_table :backoffice_core_languages_users do |t|
      t.integer :language_id
      t.integer :user_id
    end
    execute 'ALTER TABLE `backoffice_core_languages_users` 
               ADD CONSTRAINT `fk_languages_users_language` 
               FOREIGN KEY (`language_id`) REFERENCES `backoffice_core_languages`(`id`) ON DELETE CASCADE ON UPDATE CASCADE'
    execute 'ALTER TABLE `backoffice_core_languages_users` 
               ADD CONSTRAINT `fk_languages_users_user` 
               FOREIGN KEY (`user_id`) REFERENCES `backoffice_core_users`(`id`) ON DELETE CASCADE ON UPDATE CASCADE'
    execute 'INSERT INTO backoffice_core_languages (`title`) 
               VALUES ("Cesky"),("Slovensky"),("Polsky"),("Madarsky")'
    execute 'INSERT INTO backoffice_core_languages_users (`language_id`, `user_id`) 
               VALUES (1,4),(2,4)'
  end
  def down
    execute 'ALTER TABLE `backoffice_core_languages_users` 
               DROP FOREIGN KEY `fk_languages_users_language`'
    execute 'ALTER TABLE `backoffice_core_languages_users` 
               DROP FOREIGN KEY `fk_languages_users_user`'
    drop_table :backoffice_core_languages_users
    drop_table :backoffice_core_languages
  end
end
