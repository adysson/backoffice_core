class AddFamilyParamsToUser < ActiveRecord::Migration
  def change
      add_column :backoffice_core_users, :has_wife, :boolean
      add_column :backoffice_core_users, :has_partner, :boolean
      add_column :backoffice_core_users, :has_children, :boolean
      add_column :backoffice_core_users, :children_count, :integer
      add_column :backoffice_core_users, :has_pet, :boolean
      add_column :backoffice_core_users, :pet_race, :string      
  end
end
