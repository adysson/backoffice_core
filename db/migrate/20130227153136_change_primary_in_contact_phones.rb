class ChangePrimaryInContactPhones < ActiveRecord::Migration
  def change
    remove_column :backoffice_core_contact_phones, :primary
    add_column :backoffice_core_contact_phones, :primary_phone, :boolean
  end
end
