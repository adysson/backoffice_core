class ChangePrimaryInContactEmails < ActiveRecord::Migration
  def change
    remove_column :backoffice_core_contact_emails, :primary
    add_column :backoffice_core_contact_emails, :primary_email, :boolean
  end
end
