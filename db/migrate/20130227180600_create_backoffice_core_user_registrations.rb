class CreateBackofficeCoreUserRegistrations < ActiveRecord::Migration
  def up
    create_table :backoffice_core_user_registrations do |t|
      t.string :title
    end
    add_column :backoffice_core_users, :user_registration_id, :integer
    execute 'ALTER TABLE `backoffice_core_users`
               ADD CONSTRAINT `fk_user_registration`
               FOREIGN KEY (`user_registration_id`)
               REFERENCES `backoffice_core_user_registrations`(`id`)'
    execute 'INSERT INTO `backoffice_core_user_registrations` (`id`,`title`) 
               VALUES (1,"osobni"),(2,"firemni")'
  end
  
  def down
    execute 'ALTER TABLE `backoffice_core_users`
               DROP FOREIGN KEY `fk_user_registration`'
    remove_column :backoffice_core_users, :user_registration_id
    drop_table :backoffice_core_user_registrations
  end
end
