class AddLastLoggedToUsers < ActiveRecord::Migration
  def change
    add_column :backoffice_core_users, :last_logged, :timestamp, default: nil
  end
end
