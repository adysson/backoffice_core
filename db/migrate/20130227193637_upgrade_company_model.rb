class UpgradeCompanyModel < ActiveRecord::Migration
  def up
    add_column :backoffice_core_companies, :node_company_id, :integer
    add_column :backoffice_core_companies, :title, :string
    add_column :backoffice_core_companies, :ico, :string
    add_column :backoffice_core_companies, :dic, :string
    add_column :backoffice_core_companies, :street, :string
    add_column :backoffice_core_companies, :orientation_number, :string
    add_column :backoffice_core_companies, :house_number, :string
    add_column :backoffice_core_companies, :city, :string
    add_column :backoffice_core_companies, :postal_code, :string
    add_column :backoffice_core_companies, :phone, :string
    add_column :backoffice_core_companies, :cell_phone, :string
    add_column :backoffice_core_companies, :fax, :string
    add_column :backoffice_core_companies, :email, :string
    add_column :backoffice_core_companies, :www, :string
    add_column :backoffice_core_companies, :facebook, :string
    add_column :backoffice_core_companies, :icq, :string
    add_column :backoffice_core_companies, :skype, :string
    add_column :backoffice_core_companies, :description, :text
    execute 'ALTER TABLE `backoffice_core_employee_posts`
               ADD CONSTRAINT `fk_employee_posts_company`
               FOREIGN KEY (`company_id`)
               REFERENCES `backoffice_core_companies` (`id`)'
  end

  def down
    execute 'ALTER TABLE `backoffice_core_employee_posts`
               DROP FOREIGN KEY `fk_employee_posts_company`'
    remove_columns :backoffice_core_companies, :node_company_id, :title, :ico, :dic, 
                                               :street, :orientation_number, :house_number, :city, :postal_code,
                                               :phone, :cell_phone, :fax, :email, :www, :facebook, :icq, :skype, :description
  end
end
