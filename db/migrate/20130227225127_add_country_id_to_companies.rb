class AddCountryIdToCompanies < ActiveRecord::Migration
  def up
    add_column :backoffice_core_companies, :country_id, :integer
    execute 'ALTER TABLE `backoffice_core_companies`
               ADD CONSTRAINT `fk_companies_country_id`
               FOREIGN KEY (`country_id`)
               REFERENCES `backoffice_core_countries` (`id`) ON DELETE SET NULL ON UPDATE CASCADE'
  end
  
  def down
    execute 'ALTER TABLE `backoffice_core_companies`
               DROP FOREIGN KEY `fk_companies_country_id`'
    remove_column :backoffice_core_companies, :country_id
  end
end
