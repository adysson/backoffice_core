class ChangeFacebookToFbInCompanies < ActiveRecord::Migration
  def up
    remove_column :backoffice_core_companies, :facebook
    add_column :backoffice_core_companies, :fb, :string
  end

  def down
    remove_column :backoffice_core_companies, :fb
    add_column :backoffice_core_companies, :facebook, :string
  end
end
