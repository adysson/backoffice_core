class CreateBackofficeCoreCompanyDetails < ActiveRecord::Migration
  def up
    create_table :backoffice_core_company_details do |t|
      t.string :title
      t.integer :company_id
      t.integer :company_detail_type_id
    end
    add_index :backoffice_core_company_details, [:company_id, :company_detail_type_id], unique: true, name: 'company_id_company_detail_type_id_unique'
    execute 'ALTER TABLE `backoffice_core_company_details`
               ADD CONSTRAINT `fk_company_details`
               FOREIGN KEY (`company_id`)
               REFERENCES `backoffice_core_companies` (`id`) ON DELETE CASCADE ON UPDATE CASCADE'
  end
  
  def down
    execute 'ALTER TABLE `backoffice_core_company_details`
               DROP FOREIGN KEY `fk_company_details`'    
    drop_table :backoffice_core_company_details
  end
end
