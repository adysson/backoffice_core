class CreateBackofficeCoreCompanyDetailTypes < ActiveRecord::Migration
  def up
    create_table :backoffice_core_company_detail_types do |t|
      t.string :title
    end
    execute 'ALTER TABLE `backoffice_core_company_details`
               ADD CONSTRAINT `fk_company_detail_type`
               FOREIGN KEY (`company_detail_type_id`)
               REFERENCES `backoffice_core_company_detail_types` (`id`)
               ON DELETE SET NULL ON UPDATE CASCADE'
  end
  
  def down
    execute 'ALTER TABLE `backoffice_core_company_details`
               DROP FOREIGN KEY `fk_company_detail_type`'
    drop_table :backoffice_core_company_detail_types
  end
end
