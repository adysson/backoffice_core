class AddCompanyIdToCompanyDetailTypes < ActiveRecord::Migration
  def up
    add_column :backoffice_core_company_detail_types, :company_id, :integer
    add_index :backoffice_core_company_detail_types, [:title, :company_id], unique: true, name: 'company_detail_type_unique'
    execute 'ALTER TABLE `backoffice_core_company_detail_types`
               ADD CONSTRAINT `fk_company_detail_type_company`
               FOREIGN KEY (`company_id`)
               REFERENCES `backoffice_core_companies` (`id`) ON DELETE CASCADE ON UPDATE CASCADE'
  end
  
  def down
    execute 'ALTER TABLE `backoffice_core_company_detail_types`
               DROP FOREIGN KEY `fk_company_detail_type_company`'
    remove_index :backoffice_core_company_detail_types, name: :company_detail_type_unique
    remove_column :backoffice_core_company_detail_types, :company_id
  end
end
