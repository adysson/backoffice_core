class ChangeTitleToDetailValueInCompanyDetails < ActiveRecord::Migration
  def up
    remove_column :backoffice_core_company_details, :title
    add_column :backoffice_core_company_details, :detail_value, :string
  end

  def down
    remove_column :backoffice_core_company_details, :detail_value
    add_column :backoffice_core_company_details, :title, :string
  end
end
