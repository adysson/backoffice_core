class AddRegisteredByChiefToUsers < ActiveRecord::Migration
  def change
    add_column :backoffice_core_users, :registered_by_chief, :boolean, default: nil
  end
end
