class CreateBackofficeCoreGroups < ActiveRecord::Migration
  def up
    execute "CREATE TABLE `backoffice_core_groups` (
               `id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
               `title` VARCHAR(50) NOT NULL,
               `description` TEXT NULL,
               `main_admin_user_id` INT NULL,
               `admin_add_user` TINYINT(1) NULL,
               `admin_remove_user` TINYINT(1) NULL,
               `admin_destroy_group` TINYINT(1) NULL,
               `automatic` TINYINT(1) NULL,
               `automatic_company_id` INT NULL,
               `created_at` DATETIME NOT NULL,
               `updated_at` DATETIME NOT NULL
             ) COMMENT='' COLLATE 'utf8_general_ci'"
    execute "ALTER TABLE `backoffice_core_groups`
             ADD CONSTRAINT `fk_group_main_admin`
             FOREIGN KEY (`main_admin_user_id`)
             REFERENCES `backoffice_core_users` (`id`)
             ON DELETE SET NULL ON UPDATE CASCADE"
    execute "ALTER TABLE `backoffice_core_groups`
             ADD CONSTRAINT `fk_group_automatic_company`
             FOREIGN KEY (`automatic_company_id`)
             REFERENCES `backoffice_core_companies` (`id`)
             ON DELETE CASCADE ON UPDATE CASCADE"
  end
  
  def down
    execute "ALTER TABLE `backoffice_core_groups`
               DROP FOREIGN KEY `fk_group_main_admin`"
    execute "ALTER TABLE `backoffice_core_groups`
               DROP FOREIGN KEY `fk_group_automatic_company`"
    drop_table :backoffice_core_groups
  end
end
