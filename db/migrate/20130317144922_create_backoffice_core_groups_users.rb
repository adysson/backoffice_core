class CreateBackofficeCoreGroupsUsers < ActiveRecord::Migration
  def up
    create_table :backoffice_core_groups_users do |t|
      t.integer :group_id
      t.integer :user_id
      t.boolean :admin, default: nil
      t.timestamps
    end
    
    execute "ALTER TABLE `backoffice_core_groups_users`
               ADD CONSTRAINT `fk_groups`
               FOREIGN KEY (`group_id`)
               REFERENCES `backoffice_core_groups` (`id`)
               ON DELETE CASCADE ON UPDATE CASCADE"
    execute "ALTER TABLE `backoffice_core_groups_users`
               ADD CONSTRAINT `fk_users`
               FOREIGN KEY (`user_id`)
               REFERENCES `backoffice_core_users` (`id`)
               ON DELETE CASCADE ON UPDATE CASCADE"
    execute "ALTER TABLE `backoffice_core_groups_users`
               ADD UNIQUE `user_in_group` (`group_id`, `user_id`)"
  end

  def down
    execute "ALTER TABLE `backoffice_core_groups_users`
               DROP FOREIGN KEY `fk_groups`"
    execute "ALTER TABLE `backoffice_core_groups_users`
               DROP FOREIGN KEY `fk_users`"  
    drop_table :backoffice_core_groups_users
  end
end
