Rails.application.routes.draw do
  root to: "home#index"
  mount BackofficeCore::Engine => "/backoffice_core"
end
