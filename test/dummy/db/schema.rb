# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20130317151728) do

  create_table "backoffice_core_cities", :force => true do |t|
    t.string "title"
  end

  create_table "backoffice_core_companies", :force => true do |t|
    t.string  "title"
    t.string  "ico"
    t.string  "dic"
    t.string  "street"
    t.string  "orientation_number"
    t.string  "house_number"
    t.string  "city"
    t.string  "postal_code"
    t.string  "phone"
    t.string  "cell_phone"
    t.string  "fax"
    t.string  "email"
    t.string  "www"
    t.string  "icq"
    t.string  "skype"
    t.text    "description"
    t.integer "company_type_id"
    t.integer "country_id"
    t.string  "fb"
    t.integer "parent_company_id"
  end

  add_index "backoffice_core_companies", ["company_type_id"], :name => "fk_company_types_company"
  add_index "backoffice_core_companies", ["country_id"], :name => "fk_companies_country_id"
  add_index "backoffice_core_companies", ["parent_company_id"], :name => "parent_company_id"

  create_table "backoffice_core_companies_groups", :force => true do |t|
    t.integer  "group_id"
    t.integer  "company_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "backoffice_core_companies_groups", ["company_id"], :name => "fk_companies_groups_company"
  add_index "backoffice_core_companies_groups", ["group_id", "company_id"], :name => "company_in_group", :unique => true

  create_table "backoffice_core_company_detail_types", :force => true do |t|
    t.string  "title"
    t.integer "company_id"
  end

  add_index "backoffice_core_company_detail_types", ["company_id"], :name => "fk_company_detail_type_company"
  add_index "backoffice_core_company_detail_types", ["title", "company_id"], :name => "company_detail_type_unique", :unique => true

  create_table "backoffice_core_company_details", :force => true do |t|
    t.integer "company_id"
    t.integer "company_detail_type_id"
    t.string  "detail_value"
  end

  add_index "backoffice_core_company_details", ["company_detail_type_id"], :name => "company_detail_type_id"
  add_index "backoffice_core_company_details", ["company_id", "company_detail_type_id"], :name => "company_id_company_detail_type_id", :unique => true

  create_table "backoffice_core_company_types", :force => true do |t|
    t.string "title"
  end

  create_table "backoffice_core_contact_emails", :force => true do |t|
    t.string  "email",         :limit => 150, :null => false
    t.string  "email_usage",   :limit => 10
    t.string  "note"
    t.integer "user_id",                      :null => false
    t.boolean "primary_email"
  end

  add_index "backoffice_core_contact_emails", ["user_id"], :name => "user_id"

  create_table "backoffice_core_contact_phones", :force => true do |t|
    t.string  "phone",         :limit => 15, :null => false
    t.string  "phone_type",    :limit => 11, :null => false
    t.string  "phone_usage",   :limit => 10, :null => false
    t.string  "note"
    t.integer "user_id",                     :null => false
    t.boolean "primary_phone"
  end

  add_index "backoffice_core_contact_phones", ["user_id"], :name => "user_id"

  create_table "backoffice_core_countries", :force => true do |t|
    t.string "title"
  end

  create_table "backoffice_core_employee_posts", :force => true do |t|
    t.integer "company_id"
    t.string  "title"
    t.integer "primary_post"
  end

  add_index "backoffice_core_employee_posts", ["company_id"], :name => "company_id"

  create_table "backoffice_core_groups", :force => true do |t|
    t.string   "title",                :limit => 50, :null => false
    t.text     "description"
    t.integer  "main_admin_user_id"
    t.boolean  "admin_add_user"
    t.boolean  "admin_remove_user"
    t.boolean  "admin_destroy_group"
    t.boolean  "automatic"
    t.integer  "automatic_company_id"
    t.datetime "created_at",                         :null => false
    t.datetime "updated_at",                         :null => false
  end

  add_index "backoffice_core_groups", ["automatic_company_id"], :name => "fk_group_automatic_company"
  add_index "backoffice_core_groups", ["id", "title"], :name => "id_title", :unique => true
  add_index "backoffice_core_groups", ["main_admin_user_id"], :name => "fk_group_main_admin"

  create_table "backoffice_core_groups_users", :force => true do |t|
    t.integer  "group_id"
    t.integer  "user_id"
    t.boolean  "admin"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "backoffice_core_groups_users", ["group_id", "user_id"], :name => "user_in_group", :unique => true
  add_index "backoffice_core_groups_users", ["user_id"], :name => "fk_users"

  create_table "backoffice_core_languages", :force => true do |t|
    t.string "title"
  end

  create_table "backoffice_core_languages_users", :force => true do |t|
    t.integer "language_id"
    t.integer "user_id"
  end

  add_index "backoffice_core_languages_users", ["language_id"], :name => "fk_languages_users_language"
  add_index "backoffice_core_languages_users", ["user_id"], :name => "user_id"

  create_table "backoffice_core_licenses", :force => true do |t|
    t.string   "title",        :limit => 30
    t.string   "license_type", :limit => 5
    t.integer  "price"
    t.string   "duration",     :limit => 5,  :default => "year"
    t.datetime "created_at",                                     :null => false
    t.datetime "updated_at",                                     :null => false
  end

  create_table "backoffice_core_licenses_roles", :force => true do |t|
    t.integer  "license_id"
    t.integer  "role_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "backoffice_core_licenses_roles", ["license_id", "role_id"], :name => "uk_licenses_roles", :unique => true
  add_index "backoffice_core_licenses_roles", ["role_id"], :name => "fk_licenses_roles_role"

  create_table "backoffice_core_logs", :force => true do |t|
    t.string   "title"
    t.string   "description"
    t.integer  "user_id"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  add_index "backoffice_core_logs", ["user_id"], :name => "user_id"

  create_table "backoffice_core_nationalities", :force => true do |t|
    t.string "title"
  end

  create_table "backoffice_core_roles", :force => true do |t|
    t.string   "name",       :limit => 50
    t.string   "role_type",  :limit => 50
    t.datetime "created_at",               :null => false
    t.datetime "updated_at",               :null => false
  end

  create_table "backoffice_core_roles_users", :force => true do |t|
    t.integer  "user_id"
    t.integer  "role_id"
    t.boolean  "automatic",  :default => false
    t.datetime "created_at",                    :null => false
    t.datetime "updated_at",                    :null => false
  end

  add_index "backoffice_core_roles_users", ["role_id"], :name => "fk_users_roles_role"
  add_index "backoffice_core_roles_users", ["user_id", "role_id"], :name => "uk_users_roles", :unique => true

  create_table "backoffice_core_user_registrations", :force => true do |t|
    t.string "title"
  end

  create_table "backoffice_core_users", :force => true do |t|
    t.string   "email"
    t.string   "password_digest"
    t.string   "renew_password_token"
    t.string   "name"
    t.string   "surname"
    t.string   "degree_before"
    t.string   "degree_after"
    t.string   "street"
    t.string   "house_number"
    t.string   "orientation_number"
    t.string   "postal_code"
    t.integer  "country_id"
    t.integer  "nationality_id"
    t.integer  "license_id"
    t.date     "birth_date"
    t.string   "ico"
    t.string   "dic"
    t.string   "www"
    t.string   "skype"
    t.string   "icq"
    t.string   "twitter"
    t.string   "fb"
    t.string   "motto"
    t.string   "description"
    t.datetime "created_at",                                            :null => false
    t.datetime "updated_at",                                            :null => false
    t.boolean  "active",                             :default => false
    t.string   "activation_code",      :limit => 40
    t.date     "license_expiration"
    t.string   "evidence_number",      :limit => 20
    t.integer  "employee_post_id"
    t.integer  "company_id"
    t.boolean  "has_wife"
    t.boolean  "has_partner"
    t.boolean  "has_children"
    t.integer  "children_count"
    t.boolean  "has_pet"
    t.string   "pet_race"
    t.string   "city"
    t.integer  "user_registration_id"
    t.datetime "last_logged"
    t.boolean  "registered_by_chief"
    t.boolean  "show_degree_before"
    t.boolean  "show_degree_after"
    t.boolean  "show_evidence_number"
    t.boolean  "show_company"
    t.boolean  "show_branch_office"
    t.boolean  "show_address"
    t.boolean  "show_motto"
    t.boolean  "show_description"
  end

  add_index "backoffice_core_users", ["company_id"], :name => "company_id"
  add_index "backoffice_core_users", ["country_id"], :name => "fk_countries_users"
  add_index "backoffice_core_users", ["employee_post_id"], :name => "employee_post_id"
  add_index "backoffice_core_users", ["evidence_number"], :name => "index_backoffice_core_users_on_evidence_number", :unique => true
  add_index "backoffice_core_users", ["license_id"], :name => "fk_users_licenses"
  add_index "backoffice_core_users", ["nationality_id"], :name => "fk_nationalities_users"
  add_index "backoffice_core_users", ["user_registration_id"], :name => "fk_user_registration"

end
