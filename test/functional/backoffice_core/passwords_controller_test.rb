require 'test_helper'

module BackofficeCore
  class PasswordsControllerTest < ActionController::TestCase
    test "should get new" do
      get :new
      assert_response :success
    end
  
    test "should get request_password" do
      get :request_password
      assert_response :success
    end
  
  end
end
